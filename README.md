# Image Processing Huffman Coding and Motion Estimation Using Full Search Method and Mean Absolute Difference

Huffman Coding in "HuffmanCoding.c" and Motion Estimation using Full Search Algorithm and Mean Absolute Difference (MAD) measure in "MotionEstimation.c".
File "huffman_coding_for_input.txt" is manually constructed Huffman tree based on output of Huffman Coding program for input image "lenna.pgm".

Implemented in C.

My lab assignment in Multimedia Architecture and Systems, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2021