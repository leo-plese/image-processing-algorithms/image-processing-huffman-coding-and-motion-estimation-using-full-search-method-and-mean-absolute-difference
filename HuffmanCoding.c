#include <stdio.h>
#include <stdlib.h>
#define IMG_DIM 512
#define NUM_GROUPS 16


int main() {
    FILE *fil = fopen("lenna.pgm", "rb");

    if (!fil) {
        perror("could not open file");
        return 1;
    }

    unsigned int num_rgb = 0;
    int **pix_arr = (int**) malloc(IMG_DIM * sizeof(int *));
    for (int i=0; i<IMG_DIM; i++) {
        pix_arr[i] = (int*) malloc(IMG_DIM * sizeof(int));
    }

    int ch;
    int newline_cnt=0;
    while ((ch=fgetc(fil)) != EOF) {
        if (ch == '\n') {
            newline_cnt++;
            if (newline_cnt == 4) {
                break;
            }
        }
    }
    int rn=0, cn=0;
    while ((ch=fgetc(fil)) != EOF) {
        num_rgb++;

        pix_arr[rn][cn] = ch;
        if (num_rgb % IMG_DIM == 0) {
            rn++;
            cn=0;
        } else {
            cn++;
        }

    }

    int freq_table[NUM_GROUPS];
    for (int i=0; i<NUM_GROUPS; i++) {
        freq_table[i] = 0;
    }

    for (int i=0; i<IMG_DIM; i++) {
        for (int j=0; j<IMG_DIM; j++) {
            int group = pix_arr[i][j] / 16;
            freq_table[group]++;
        }
    }

    for (int i=0; i<NUM_GROUPS-1; i++) {
        printf("%d %f\n",i,((double)freq_table[i])/(IMG_DIM*IMG_DIM));
    }
    printf("%d %f",NUM_GROUPS-1,((double)freq_table[NUM_GROUPS-1])/(IMG_DIM*IMG_DIM));

    fclose(fil);

    for (int i=0; i<IMG_DIM; i++) {
        free(pix_arr[i]);
    }
    free(pix_arr);

    return 0;
}
