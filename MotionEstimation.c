#include <stdio.h>
#include <stdlib.h>
#define IMG_DIM 512
#define BLOCK_DIM 16
#define TRANSL 16

double calc_mad(int row_from, int col_from, int **pix_arr, int **block_arr) {
    double mad_sim = 0;
    for (int i=0; i<BLOCK_DIM; i++) {
        for (int j=0; j<BLOCK_DIM; j++) {
            mad_sim += abs(block_arr[i][j] - pix_arr[row_from+i][col_from+j]);
        }
    }

    mad_sim /= (BLOCK_DIM*BLOCK_DIM);

    return mad_sim;
}

int **get_pix_arr(FILE *fil) {
    unsigned int num_rgb = 0;
    int **pix_arr = (int**) malloc(IMG_DIM * sizeof(int *));
    for (int i=0; i<IMG_DIM; i++) {
        pix_arr[i] = (int*) malloc(IMG_DIM * sizeof(int));
    }

    int ch;
    int newline_cnt=0;
    while ((ch=fgetc(fil)) != EOF) {
        if (ch == '\n') {
            newline_cnt++;
            if (newline_cnt == 4) {
                break;
            }
        }
    }

    int rn=0, cn=0;
    while ((ch=fgetc(fil)) != EOF) {
        num_rgb++;
        pix_arr[rn][cn] = ch;
        if (num_rgb % IMG_DIM == 0) {
            rn++;
            cn=0;
        } else {
            cn++;
        }

    }

    return pix_arr;
}

FILE *open_pix_arr_file(char *img_filename) {
    FILE *fil = fopen(img_filename, "rb");

    if (!fil) {
        perror("could not open file");
        return NULL;
    }

    return fil;
}

int main(int argc, char **argv) {
    FILE *fil_new = open_pix_arr_file("lenna1.pgm");
    if (fil_new == NULL)
        return 1;

    int **pix_arr_new = get_pix_arr(fil_new);

    FILE *fil_old = open_pix_arr_file("lenna.pgm");
    if (fil_old == NULL)
        return 1;

    int **pix_arr_old = get_pix_arr(fil_old);


    int **block_arr = (int**) malloc(BLOCK_DIM*sizeof(int*));
    for (int i=0; i<BLOCK_DIM; i++) {
        block_arr[i] = (int*) malloc(BLOCK_DIM*sizeof(int));
    }

    char *pt;
    int block_nr = strtol(argv[1], &pt, 10);
    const int NUM_BLOCKS_PER_AXIS = IMG_DIM/BLOCK_DIM;
    int block_2d_row = block_nr/NUM_BLOCKS_PER_AXIS;
    int block_2d_col = block_nr - block_2d_row*NUM_BLOCKS_PER_AXIS;
    int row_from = block_2d_row * BLOCK_DIM;
    int col_from = block_2d_col * BLOCK_DIM;
    int row_to = row_from+BLOCK_DIM;
    int col_to = col_from+BLOCK_DIM;

    int b_i = 0, b_j = 0;
    for (int r=row_from; r<row_to; r++) {
        for (int c=col_from; c<col_to; c++) {
            block_arr[b_i][b_j] = pix_arr_new[r][c];
            b_j++;
        }
        b_i++;
        b_j=0;
    }

    int begin_row_scan_from = row_from-TRANSL >= 0 ? row_from-TRANSL : 0;
    int begin_col_scan_from = col_from-TRANSL >= 0 ? col_from-TRANSL : 0;
    int begin_row_scan_to = row_to+TRANSL <= IMG_DIM ? row_from+TRANSL : IMG_DIM-BLOCK_DIM;
    int begin_col_scan_to = col_to+TRANSL <= IMG_DIM ? col_from+TRANSL : IMG_DIM-BLOCK_DIM;

    int row_min_mad = begin_row_scan_from, col_min_mad = begin_col_scan_from;
    double min_mad = calc_mad(begin_row_scan_from, begin_col_scan_from, pix_arr_old, block_arr);

    for (int i=begin_row_scan_from; i<=begin_row_scan_to; i++) {
        for (int j=begin_col_scan_from; j<=begin_col_scan_to; j++) {
            double mad = calc_mad(i, j, pix_arr_old, block_arr);
            if (mad < min_mad) {
                min_mad = mad;
                row_min_mad = i;
                col_min_mad = j;
            }
        }
    }

    int vect_x_comp = col_min_mad - col_from;
    int vect_y_comp = row_min_mad - row_from;

    printf("%d,%d", vect_x_comp, vect_y_comp);


    fclose(fil_new);
    fclose(fil_old);

    for (int i=0; i<IMG_DIM; i++) {
        free(pix_arr_new[i]);
    }
    free(pix_arr_new);

    for (int i=0; i<IMG_DIM; i++) {
        free(pix_arr_old[i]);
    }
    free(pix_arr_old);

    return 0;
}
